"""elearning URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path

from courses.views import course_list, course_detail, course_add
from students.views import student_detail

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', course_list),
    re_path(r'^course_detail/(?P<course_id>\d)$', course_detail, name='course_detail'),
    path('course_add/', course_add, name='course_add'),
    path('student_detail/<int:student_id>', student_detail),
]
